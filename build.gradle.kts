import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

plugins {
	id("org.springframework.boot") version "2.3.0.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	kotlin("jvm") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"
	kotlin("plugin.jpa") version "1.3.72"
	kotlin("kapt") version "1.3.72"
	id ("com.commercehub.gradle.plugin.avro") version "0.9.1"
	id ("org.jetbrains.kotlin.plugin.serialization") version "1.3.72"
	id ("net.ltgt.apt") version "0.8"
}

group = "com.training"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
	maven {
		setUrl("https://packages.confluent.io/maven/")
	}
	maven {
		setUrl("https://repo.spring.io/snapshot")
	}

	maven {
		setUrl("https://repo.spring.io/milestone")
	}
	jcenter()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-mustache")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.apache.kafka:kafka-streams")
	implementation("org.flywaydb:flyway-core")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.kafka:spring-kafka")
	implementation("org.apache.avro:avro:1.8.2")
	implementation ("io.confluent:kafka-avro-serializer:5.3.0")
	implementation("io.confluent:monitoring-interceptors:5.3.0")
	implementation("io.confluent:kafka-schema-registry-client:5.3.0")

	// Security
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.security.oauth:spring-security-oauth2:2.1.5.RELEASE")
	implementation("io.jsonwebtoken:jjwt-api:0.11.0")
	implementation("io.jsonwebtoken:jjwt-impl:0.11.0")
	implementation("io.jsonwebtoken:jjwt-jackson:0.11.0")

	// Embedded postgres
	// https://mvnrepository.com/artifact/io.zonky.test/embedded-database-spring-test
	implementation("io.zonky.test:embedded-database-spring-test:1.5.4")

	// https://mvnrepository.com/artifact/io.confluent/kafka-streams-avro-serde
	implementation("io.confluent:kafka-streams-avro-serde:5.2.1")

	implementation("org.apache.kafka:kafka-clients:2.5.0")
	implementation("org.apache.kafka:kafka-clients:2.5.0:test")

	// Swagger UI
	implementation("io.springfox:springfox-swagger2:2.9.2")
	implementation("io.springfox:springfox-swagger-ui:2.9.2")
	implementation("io.springfox:springfox-bean-validators:2.9.2")
	implementation("org.springframework.security:spring-security-web")

	// Mapstruct
	implementation("org.mapstruct:mapstruct:1.4.0.Beta1")
	kapt("org.mapstruct:mapstruct-processor:1.4.0.Beta1")

	developmentOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("org.postgresql:postgresql")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("org.springframework.kafka:spring-kafka-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.flywaydb.flyway-test-extensions:flyway-spring-test")
	runtimeOnly("com.h2database:h2")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
tasks.withType<com.commercehub.gradle.plugin.avro.GenerateAvroJavaTask> {
	setOutputDir(File(projectDir,"/src/main/java"))
	source(projectDir, "/src/main/resources/avro")
}