package com.training.gft.mapper;

import com.training.gft.dto.ChargeRequest;
import com.training.gft.dto.ChargeResponse;
import com.training.gft.model.Charge;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-06-12T16:19:17+0700",
    comments = "version: 1.4.0.Beta1, compiler: IncrementalProcessingEnvironment from kotlin-annotation-processing-gradle-1.3.72.jar, environment: Java 1.8.0_251 (Oracle Corporation)"
)
@Component
public class ChargeMapperImpl implements ChargeMapper {

    @Override
    public ChargeResponse convertToResponse(Charge charge) {
        if ( charge == null ) {
            return null;
        }

        long chargeId;
        String cardNumber;
        String status;
        int amount;
        String brand;

        chargeId = charge.getChargeId();
        cardNumber = charge.getCardNumber();
        status = charge.getStatus();
        amount = charge.getAmount();
        brand = charge.getBrand();

        ChargeResponse chargeResponse = new ChargeResponse( chargeId, cardNumber, status, amount, brand );

        return chargeResponse;
    }

    @Override
    public Charge convertToModel(ChargeRequest chargeRequest) {
        if ( chargeRequest == null ) {
            return null;
        }

        String cardNumber;
        String status;
        int amount;
        String brand;

        cardNumber = chargeRequest.getCardNumber();
        status = chargeRequest.getStatus();
        amount = chargeRequest.getAmount();
        brand = chargeRequest.getBrand();

        long chargeId = 0L;

        Charge charge = new Charge( chargeId, cardNumber, status, amount, brand );

        return charge;
    }
}
