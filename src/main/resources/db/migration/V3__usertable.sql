create table app_role
(
    id          integer GENERATED ALWAYS AS IDENTITY NOT NULL
        constraint app_role_pkey
            primary key,
    description varchar(255),
    role_name   varchar(255)
);

-- alter table app_role
--     owner to postgres;
create table app_user
(
    id         integer GENERATED ALWAYS AS IDENTITY NOT NULL
        constraint app_user_pkey
            primary key,
    first_name varchar(255),
    last_name  varchar(255),
    password   varchar(255),
    username   varchar(255)
);

-- alter table app_user
--     owner to postgres;

create table public.user_role
(
    user_id integer not null
        constraint fkg7fr1r7o0fkk41nfhnjdyqn7b
            references public.app_user,
    role_id integer not null
        constraint fkp6m37g6n6c288s096400uw8fw
            references public.app_role
);

-- alter table user_role
--     owner to postgres;

-- TRUNCATE TABLE app_role;
INSERT INTO app_role (role_name, description)
VALUES ('ADMIN_USER', 'Admin User - Has permission to perform admin tasks');
INSERT INTO app_role (role_name, description)
VALUES ('STANDARD_USER', 'Standard User - Has no admin rights');

-- TRUNCATE TABLE app_user;
-- password test1234
INSERT INTO app_user (first_name, last_name, password, username)
VALUES ('Admin', 'Admin', '$2a$10$5AWyzymSnNypg9BkMOyKE.zA05GtRKHCoWimh.q2w.KAO5koBYPM6', 'admin.admin');
-- password test1234
INSERT INTO app_user (first_name, last_name, password, username)
VALUES ('John', 'Doe', '$2a$10$5AWyzymSnNypg9BkMOyKE.zA05GtRKHCoWimh.q2w.KAO5koBYPM6', 'john.doe');

TRUNCATE TABLE user_role;
INSERT INTO user_role(user_id, role_id)
VALUES (1, 1);
INSERT INTO user_role(user_id, role_id)
VALUES (1, 2);
INSERT INTO user_role(user_id, role_id)
VALUES (2, 2);
