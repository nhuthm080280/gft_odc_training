package com.training.gft.config

import org.springframework.boot.context.properties.ConfigurationProperties
//import org.hibernate.validator.constraints.Length
@ConfigurationProperties(prefix = "jwt-security")
class SecurityProperties {
    //@Length(min = 42, message = "Minimum length for the secret is 42.")
    var secret = ""
    var expirationTime: Long = 31 // in days
    var tokenPrefix = "Bearer "
    var headerString = "Authorization"
    var strength = 10
}