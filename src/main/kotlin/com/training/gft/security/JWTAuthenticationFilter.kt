package com.training.gft.security

import com.training.gft.config.SecurityProperties
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.core.userdetails.User
import java.time.ZonedDateTime
import java.util.*
import kotlin.collections.ArrayList

class JWTAuthenticationFilter(
        private val authManager: AuthenticationManager,
        private val securityProperties: SecurityProperties
) : UsernamePasswordAuthenticationFilter() {

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(
            req: HttpServletRequest,
            res: HttpServletResponse?
    ): Authentication {
        return try {
            val mapper = jacksonObjectMapper()

            val creds = mapper
                    .readValue<com.training.gft.model.User>(req.inputStream)

            authManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            creds.username,
                            creds.password,
                            ArrayList<GrantedAuthority>()
                    )
            )
        } catch (e: IOException) {
            throw AuthenticationServiceException(e.message)
        }
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(
            req: HttpServletRequest,
            res: HttpServletResponse,
            chain: FilterChain?,
            auth: Authentication
    ) {
        val claims: MutableList<String> = mutableListOf()
        if (auth.authorities.isNotEmpty())
            auth.authorities.forEach { a -> claims.add(a.toString()) }

        val token = Jwts.builder()
                .setSubject((auth.principal as User).username)
                .claim("auth", claims)
                //.setExpiration(Date().add(Calendar.DAY_OF_MONTH, securityProperties.expirationTime))
                .setExpiration(Date.from(ZonedDateTime.now().plusDays(securityProperties.expirationTime).toInstant()))
                .signWith(Keys.hmacShaKeyFor(securityProperties.secret.toByteArray()), SignatureAlgorithm.HS512)
                .compact()
        res.addHeader(securityProperties.headerString, securityProperties.tokenPrefix + token)
    }
}