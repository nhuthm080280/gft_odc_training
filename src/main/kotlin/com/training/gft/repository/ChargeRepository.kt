package com.training.gft.repository

import com.training.gft.model.Charge
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ChargeRepository : PagingAndSortingRepository<Charge, Long>, JpaSpecificationExecutor<Charge> {
    fun findAllByChargeId(id: Long):Charge
}