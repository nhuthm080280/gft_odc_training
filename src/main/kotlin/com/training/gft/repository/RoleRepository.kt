package com.training.gft.repository

import com.training.gft.model.Role
import org.springframework.data.repository.CrudRepository

interface RoleRepository : CrudRepository<Role, String>
