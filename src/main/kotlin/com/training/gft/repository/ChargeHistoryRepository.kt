package com.training.gft.repository

import com.training.gft.model.ChargeHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ChargeHistoryRepository : JpaRepository<ChargeHistory, Integer>{
}