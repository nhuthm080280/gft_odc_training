package com.training.gft.mapper

import com.training.gft.dto.ChargeRequest
import com.training.gft.dto.ChargeResponse
import com.training.gft.model.Charge
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface ChargeMapper {
    fun convertToResponse(charge: Charge) : ChargeResponse
    @InheritInverseConfiguration
    fun convertToModel(chargeRequest: ChargeRequest) : Charge
}