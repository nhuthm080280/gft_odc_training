package com.training.gft.model

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;

@Entity
data class Charge (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val chargeId: Long,

        @Column(nullable = false, length = 50)
        val cardNumber: String,

        @Column(nullable = false, length = 50)
        val status: String,

        @Column(nullable = false, length = 50)
        val amount: Int,

        @Column(nullable = false, length = 50)
        val brand: String
)
{
        companion object {
                val ATTR_CHARGE_ID = "chargeId"
                val ATTR_CARD_NUMBER = "cardNumber"
                val ATTR_STATUS = "status"
                val ATTR_AMOUNT = "amount"
                val ATTR_BRAND = "brand"
        }
}