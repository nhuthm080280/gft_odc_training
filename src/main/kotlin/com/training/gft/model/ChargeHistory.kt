package com.training.gft.model

import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class ChargeHistory(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val chargeHistoryId: Long,

        @Column(nullable = false, length = 240)
        val content: String,

        @Column(nullable = false, length = 50)
        val createdTime: Date

)