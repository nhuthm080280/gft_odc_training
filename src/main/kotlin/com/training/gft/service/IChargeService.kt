package com.training.gft.service

import com.training.gft.dto.ChargeRequest
import com.training.gft.dto.ChargeResponse
import com.training.gft.model.Charge
import java.util.stream.Stream

interface IChargeService {
    fun getChargeById(chargeId: Long): ChargeResponse
    fun getAllCharges(page:Int, size:Int, sort:String, order: String): Stream<ChargeResponse>
    fun createNewCharge(request: ChargeRequest) : ChargeResponse
    fun deleteChargeById(chargeId: Long)
    fun updateChargeById(chargeId: Long, newCharge: ChargeRequest): ChargeResponse
    fun searchCharge(status: String?, amount: Int?) : MutableList<Charge>
}