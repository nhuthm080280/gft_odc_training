package com.training.gft.service

import com.training.gft.ChargeSchema
import com.training.gft.dto.ChargeRequest
import com.training.gft.dto.ChargeResponse
import com.training.gft.exception.ResourceNotFoundException
import com.training.gft.infrastructure.ChargeProducer
import com.training.gft.mapper.ChargeMapper
import com.training.gft.model.Charge
import com.training.gft.repository.ChargeRepository
import org.mapstruct.factory.Mappers
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.domain.Sort.Direction
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import java.util.stream.Stream
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

@Service
class ChargeService(private val chargeRepository: ChargeRepository,
                    private val chargeProducer: ChargeProducer) : IChargeService{
    val chargeMapper: ChargeMapper = Mappers.getMapper(ChargeMapper::class.java)
    override
    fun getAllCharges(page:Int, size:Int, sort:String, order: String): Stream<ChargeResponse> {
        val sortDirection: Direction = if (order == "DESC") Direction.DESC else Direction.ASC
        val pageable: Pageable = PageRequest.of(page, size, Sort.by(sortDirection,sort))
        val charges: Page<Charge> = chargeRepository.findAll(pageable)
        return charges.stream().map(chargeMapper::convertToResponse)
    }

    override
    fun getChargeById(chargeId: Long): ChargeResponse{
        return chargeRepository.findById(chargeId).map(chargeMapper::convertToResponse)
                .orElseThrow {throw ResourceNotFoundException("Charge not found with id:$chargeId")}
    }

    override
    fun createNewCharge(request:ChargeRequest) : ChargeResponse {
        // object mapper to entity
        val charge = chargeMapper.convertToModel(request)
        val result = chargeRepository.save(charge)
        return chargeMapper.convertToResponse(result)
    }

    override
    fun deleteChargeById(chargeId: Long) {
        chargeRepository.findById(chargeId).map { charge  ->
            chargeRepository.delete(charge)
        }.orElseThrow {throw ResourceNotFoundException("Charge not found with id:$chargeId")}
    }

    override
    fun updateChargeById(chargeId: Long, newCharge: ChargeRequest): ChargeResponse  {
        return chargeRepository.findById(chargeId).map { existingCharge ->
            val updatedCharge: Charge = existingCharge
                    .copy(cardNumber = newCharge.cardNumber, status = newCharge.status,
                            amount = newCharge.amount, brand = newCharge.brand)
            val result = chargeRepository.save(updatedCharge)
            chargeProducer.send(ChargeSchema(result.chargeId,result.cardNumber, result.status, result.amount,  result.brand))
            chargeMapper.convertToResponse(result)
        }.orElseThrow {throw ResourceNotFoundException("Charge not found with id:$chargeId")}
    }
    // Dynamic search
    fun hasStatus(status: String?): Specification<Charge?>? {
        return Specification { charge: Root<Charge?>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder -> cb.equal(charge.get<Any>(Charge::status.name), status) }
    }

    fun hasAmount(amount: Int?): Specification<Charge?>? {
        return Specification { charge: Root<Charge?>,
                               cq: CriteriaQuery<*>?,
                               cb: CriteriaBuilder -> cb.equal(charge.get<Any>(Charge::amount.name), amount) }
    }

    override
    fun searchCharge(status: String?, amount: Int?) : MutableList<Charge> {
        return chargeRepository.findAll(Specification.where(hasAmount(amount))?.and(hasStatus(status)))
    }
}