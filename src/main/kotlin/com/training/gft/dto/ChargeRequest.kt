package com.training.gft.dto

import io.swagger.annotations.ApiModelProperty

data class ChargeRequest (
        @ApiModelProperty(notes = "Max length of cardNumber is 50 characters", required = true)
        val cardNumber: String,
        @ApiModelProperty(notes = "Max length of status is 50 characters", required = true)
        val status: String,
        @ApiModelProperty(notes = "Max length of amount is 50 characters", required = true)
        val amount: Int,
        @ApiModelProperty(notes = "Max length of brand is 50 characters", required = true)
        val brand: String
)