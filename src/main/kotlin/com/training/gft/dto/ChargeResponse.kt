package com.training.gft.dto

data class ChargeResponse(
        val chargeId: Long,
        val cardNumber: String,
        val status: String,
        val amount: Int,
        val brand: String
)