package com.training.gft.infrastructure

import com.training.gft.ChargeSchema
import org.apache.avro.Schema
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.io.File

@Component
class ChargeProducer(private val kafkaTemplate: KafkaTemplate<String, ChargeSchema>) {
    private val logger = LoggerFactory.getLogger(javaClass)
    @Value("\${app.topic.charge}")
    private val topic: String? = null
    fun send(chargeSchema: ChargeSchema) {
        if (topic != null) {
            this.kafkaTemplate.send(topic, chargeSchema.getCardNumber(), chargeSchema)
            logger.info("Send data to topic {} with data {}", topic, chargeSchema)
        }
    }
}