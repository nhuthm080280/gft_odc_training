package com.training.gft.infrastructure

import com.training.gft.ChargeSchema
import com.training.gft.model.ChargeHistory
import com.training.gft.repository.ChargeHistoryRepository
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component
import java.util.Date

@Component
class ChargeConsumer(private val chargeHistoryRepository: ChargeHistoryRepository) {
    private val logger = LoggerFactory.getLogger(javaClass)
    @KafkaListener(topics = ["\${app.topic.charge}"], groupId = "\${kafka.group-id}")
    fun processMessage(record: ConsumerRecord<String, ChargeSchema>) {
        chargeHistoryRepository.save(ChargeHistory(0, record.value().toString(), Date()));
        logger.info("got message: {}", record.value().toString())
    }
}