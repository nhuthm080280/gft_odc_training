package com.training.gft.controller

import com.training.gft.dto.ChargeRequest
import com.training.gft.dto.ChargeResponse
import com.training.gft.model.Charge
import com.training.gft.service.IChargeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.stream.Stream

@RestController
@RequestMapping("/charge")
@Api(value = "Charge Information", tags = ["Charge"])
class ChargeController(private val chargeService: IChargeService) {
    @ApiOperation(value = "All charges Information")
    @GetMapping("/")
    fun getAllCharges(@RequestParam page:Int,
                      @RequestParam size:Int,
                      @RequestParam sort:String,
                      @RequestParam order:String): Stream<ChargeResponse> = chargeService.getAllCharges(page,size, sort, order)

    @ApiOperation(value = "Get charge information by chargeId")
    @GetMapping("/{chargeId}")
    fun getChargeById(@PathVariable(value = "chargeId") chargeId: Long): ResponseEntity<ChargeResponse> {
        return ResponseEntity.ok().body(chargeService.getChargeById(chargeId))
    }

    @ApiOperation(value = "Create charge")
    @PostMapping("/")
    fun createNewCharge(@Validated @RequestBody request: ChargeRequest): ChargeResponse {
        return chargeService.createNewCharge(request)
    }

    @ApiOperation(value = "Delete charge. Admin role is required.")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @DeleteMapping("/{chargeId}")
    fun deleteChargeById(@PathVariable(value = "chargeId") chargeId: Long) = chargeService.deleteChargeById(chargeId)

    @ApiOperation(value = "Update charge information based on chargeId")
    @PutMapping("/{chargeId}")
    fun updateChargeById(@PathVariable(value = "chargeId") chargeId: Long,
                         @RequestBody request: ChargeRequest): ResponseEntity<ChargeResponse> {
        return ResponseEntity.ok().body(chargeService.updateChargeById(chargeId, request))
    }

    @GetMapping("/search")
    fun searchCharges(@RequestParam status: String,
                      @RequestParam amount: Int): List<Charge> = chargeService.searchCharge(status, amount)
}