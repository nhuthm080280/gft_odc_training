package com.training.gft.exception

class ResourceNotFoundException (override val message: String?) : Exception(message)