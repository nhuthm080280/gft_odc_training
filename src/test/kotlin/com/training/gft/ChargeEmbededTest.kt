package com.training.gft

import com.training.gft.model.Charge
import com.training.gft.repository.ChargeRepository
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class SpringDataJpaAnnotationTest(@Autowired
                                  private val chargeRepository: ChargeRepository) {

    @Test
    fun testFindChargeByIdSuccess() {
        val charge: Charge = chargeRepository?.findAllByChargeId(1L)
        assertThat(charge.cardNumber).isEqualTo("1990")
    }

    @Test
    fun testFindChargeByIdFailed() {
        val charge: Charge = chargeRepository?.findAllByChargeId(9L)
        assertThat(charge.cardNumber).isEqualTo("1990")
    }

}