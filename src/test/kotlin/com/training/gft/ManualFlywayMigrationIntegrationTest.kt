package com.training.gft

import org.flywaydb.core.Flyway
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ManualFlywayMigrationIntegrationTest {
    @Autowired
    private val flyway: Flyway? = null

    @Test
    fun skipAutomaticAndTriggerManualFlywayMigration() {
        flyway?.migrate()
    }
}