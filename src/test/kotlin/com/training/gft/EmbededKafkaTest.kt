package com.training.gft

import com.training.gft.infrastructure.ChargeProducer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.test.EmbeddedKafkaBroker
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.kafka.test.utils.KafkaTestUtils
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension

@EmbeddedKafka(partitions = 3, topics = ["charge-topic"], brokerProperties = ["listeners=PLAINTEXT://localhost:9092", "port=9092"])
@ExtendWith(SpringExtension::class)
@SpringBootTest
@DirtiesContext
class EmbededKafkaTest() {
    @Autowired
    private lateinit var chargeProducer: ChargeProducer
    @Autowired
    private lateinit var embeddedKafka: EmbeddedKafkaBroker

    @Test
    fun testChargeProducer(){
        // TODO test with embedded schema registry
        val chargeSchema =  ChargeSchema(1,"009","ACTIVE",10,"HSBC")
        val senderProps = KafkaTestUtils.producerProps(embeddedKafka.brokersAsString)
        val template = KafkaTemplate(DefaultKafkaProducerFactory<Int, String>(senderProps))
        template.defaultTopic = "charge-topic"
        template.sendDefault("simple test")
        //chargeProducer.send(chargeSchema)
    }
}