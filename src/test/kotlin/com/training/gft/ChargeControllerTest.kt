package com.training.gft

import com.training.gft.infrastructure.ChargeProducer
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.flywaydb.core.Flyway
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureEmbeddedDatabase
class ChargeControllerTest {
    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var flyway: Flyway

    @MockBean
    private lateinit var chargeProducer: ChargeProducer

    @BeforeEach
    fun init() {
        flyway.clean()
        flyway.migrate()
    }

    @AfterEach
    fun cleanAfterEach() {
        flyway.clean()
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testGetChargeByIdSuccess() {
        mvc.perform(MockMvcRequestBuilders.get("/charge/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testGetChargeByIdFailed() {
        mvc.perform(MockMvcRequestBuilders.get("/charge/99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.jsonPath("details").value("Charge not found with id:99"))
                .andExpect(MockMvcResultMatchers.jsonPath("message").value("Resource Not Found"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testCreateChargeFailed() {

        val body = "test"

        mvc.perform(MockMvcRequestBuilders.post("/charge/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testCreateChargeSuccess() {
        val body = "{\n" +
                "  \"cardNumber\": \"123\",\n" +
                "  \"status\" : \"active\",\n" +
                "  \"amount\" : 10,\n" +
                "  \"brand\" : \"HSBC\"\n" +
                "}"
        mvc.perform(MockMvcRequestBuilders.post("/charge/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testUpdateChargeSuccess() {
        val body = "{\n" +
                "  \"cardNumber\": \"123\",\n" +
                "  \"status\" : \"active\",\n" +
                "  \"amount\" : 10,\n" +
                "  \"brand\" : \"HSBC\"\n" +
                "}"
        mvc.perform(MockMvcRequestBuilders.put("/charge/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "john.doe", authorities = ["STANDARD_USER"])
    fun testGetChargesSuccess() {
        mvc.perform(MockMvcRequestBuilders.get("/charge/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "admin.admin", authorities = ["ADMIN_USER"])
    fun testDeleteChargeSuccess() {
        mvc.perform(MockMvcRequestBuilders.delete("/charge/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

}